<?php
class User
{
    //Id
    public $Id;
    //Username
    public $Username;
    //First Name
    public $Surname;
    //Family Name
    public $Name;
    //Email
    public $Email;
    //Password
    public $Password;
    //Returns Full Name (Surname Name)
    public function Fullname()
    {
        return $this->Surname . " " . $this->Name;
    }
    //Fills out a User
    public static function FillUser(array $userArray)
    {
        $extract = new User();
        if (isset($userArray["Id"])) {
            $extract->Id = $userArray["Id"];
        }
        if (isset($userArray["Username"])) {
            $extract->Username = $userArray["Username"];
        }
        if (isset($userArray["Surname"])) {
            $extract->Surname = $userArray["Surname"];
        }
        if (isset($userArray["Name"])) {
            $extract->Name = $userArray["Name"];
        }
        if (isset($userArray["Email"])) {
            $extract->Email = $userArray["Email"];
        }
        if (isset($userArray["Password"])) {
            $extract->Password = $userArray["Password"];
        }
        return $extract;
    }
}
?>