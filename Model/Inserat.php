<?php
include_once "../Database/Database.php";
class Inserat{
    //Id
    public $Id;
    //Title
    public $Title;
    //Description
    public $Description;
    //Price
    public $Price;
    //Owner Id
    public $OwnerId;
    //Owner
    public function Owner()
    {
        $db = new Database();
        $sql = "SELECT * FROM user WHERE Id = ?";
        $result = $db->ExtractData($db->resultStatement($sql, "i", array($this->OwnerId)), "User");
        return $result[0];
    }
    //Buyer Id
    public $BuyerId;
    //Buyer
    public function Buyer()
    {
        if (isset($this->BuyerId)){
            $db = new Database();
            $sql = "SELECT * FROM user WHERE Id = ?";
            $result = $db->ExtractData($db->resultStatement($sql, "i", array($this->BuyerId)), "User");
            return $result[0];
        }
        return null;
    }
    //Fills out a Inserat
    public static function FillInserat(array $inseratArray)
    {
        $extract = new Inserat();
        if (isset($inseratArray["Id"])) {
            $extract->Id = $inseratArray["Id"];
        }
        if (isset($inseratArray["Title"])){
            $extract->Title = $inseratArray["Title"];
        }
        if (isset($inseratArray["Description"])){
            $extract->Description = $inseratArray["Description"];
        }
        if (isset($inseratArray["Price"])){
            $extract->Price = $inseratArray["Price"];
        }
        if (isset($inseratArray["OwnerId"])){
            $extract->OwnerId = $inseratArray["OwnerId"];
        }
        if (isset($inseratArray["BuyerId"])){
            $extract->BuyerId = $inseratArray["BuyerId"];
        }
        return $extract;
    }
}
?>