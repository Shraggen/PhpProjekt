<?php
include_once "../Controller/AuthenticationController.php";
include_once "../Controller/InseratController.php";
session_start();
$inseratControl = new InseratController();


if (!AuthenticationController::CheckAuthentication()) {
    exit(header("RegisterView.php"));
} else if (!empty($_POST) && AuthenticationController::CheckCSRF($_POST)) {
    $inseratControl->PostInserat(InseratController::PopulateInserat($_POST));
    exit(header("Location: HomeView.php"));
}
?>
<html>
<head>
    <link rel="stylesheet" href="../Context/bootstrap.css">
</head>
<body>
<div class="container">
    <h1>Create Inserat</h1>
    <nav class="navbar">
        <a href="HomeView.php">Home</a>
        <a href="UserProfileView.php">My Profile</a>
        <a href="Logout.php">Logout</a>
    </nav>
    <form method="post">
        <input type="hidden" name="token" value="<?php echo $_SESSION["token"] ?>">
        <div class="form-group">
            <label for="Title">Title</label>
            <input class="form-control" id="Title" type="text" name="Title">
        </div>
        <div class="form-group">
            <label for="Price">Price</label>
            <input class="form-control" id="Price" type="number" name="Price">
        </div>
        <div class="form-group">
            <label for="Description">Description</label>
            <input class="form-control" id="Description" type="text" name="Description">
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </form>
</div>
</body>
</html>
