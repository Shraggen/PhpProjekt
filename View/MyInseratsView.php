<?php
include_once "../Controller/AuthenticationController.php";
include_once "../Controller/InseratController.php";
session_start();
$inseratController = new InseratController();

if (!AuthenticationController::CheckAuthentication()) {
    exit(header("Location: RegisterView.php"));
}
?>
<html>
<head>
    <link href="../Context/bootstrap.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h1>My Inserats</h1>
    <nav class="navbar">
        <a href="HomeView.php">Home</a>
        <a href="CreateInseratView.php">Create New Inserat</a>
        <a href="Logout.php">Logout</a>
    </nav>
    <h2>Personal</h2>
    <ul class="list-group">
        <?php
        $inserate = $inseratController->SearchMyInserate();
        for ($i = 0; $i < count($inserate); $i++) {
            printf("<li class='list-group-item'><a class='float-left' href='EditInseratView.php?Id=%d'>%s<a class='btn btn-danger float-right' href='DeleteInserat.php?Id=%d' onclick='return confirmDeletion()'>Delete</a></a></li>", $inserate[$i]->Id, $inserate[$i]->Title, $inserate[$i]->Id);
        }
        ?>
    </ul>
    <h2>Bought</h2>
    <ul class="list-group">
        <?php
        $boughtInserate = $inseratController->SearchBoughtInserate();
        for ($i = 0; $i < count($boughtInserate); $i++) {
            printf("<li class='list-group-item'><a class='float-left' href='EditInseratView.php?Id=%d'>%s</a></li>", $boughtInserate[$i]->Id, $boughtInserate[$i]->Title);
        }
        ?>
    </ul>
</div>
<script>
    function confirmDeletion() {
        return confirm("Are you sure you want to delete this inserat");
    }
</script>
</body>
</html>
