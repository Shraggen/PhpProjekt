<?php
include_once "../Controller/AuthenticationController.php";
include_once "../Controller/UserController.php";
session_start();

$userControll = new UserController();

$user;

if (!AuthenticationController::CheckAuthentication()) {
    exit(header("RegisterView.php"));
} elseif (isset($_SESSION["UserId"])) {
    $user = $userControll->SearchUser((int)$_SESSION["UserId"]);
} else {
    exit(header("Location: HomeView.php"));
}
?>
<html>
<head>
    <link rel="stylesheet" href="../Context/bootstrap.css">
</head>
<body>
<div class="container">
    <h1>Profile</h1>
    <nav class="navbar">
        <a href="HomeView.php">Home</a>
        <a href="MyInseratsView.php">My Inserats</a>
        <a href="Logout.php">Logout</a>
    </nav>
    <div class="jumbotron">
        <?php
        echo "<h1>" . $user->Fullname() . "</h1>";
        echo "<h1>" . $user->Username . "</h1>";
        echo "<h1>" . $user->Email . "</h1>";
        ?>
        <form method="post" action="DeleteUser.php">
            <input type="hidden" name="token" value="<?php echo $_SESSION["token"] ?>">
            <button class="btn btn-danger" type="submit"
                    onclick="return confirm('Are you sure you want to delete your User')" type="submit">Delete User
            </button>
        </form>
        <a class="btn btn-primary" href="EditUserView.php?Id=<?php echo $_SESSION["UserId"] ?>">Edit User</a>
    </div>
</div>
</body>
</html>
