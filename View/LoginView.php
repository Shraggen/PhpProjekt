<?php
include_once "../Controller/AuthenticationController.php";
include_once "../Controller/UserController.php";

$auth = new AuthenticationController();

session_start();
error_reporting(0);
if (AuthenticationController::CheckAuthentication()) {
    header("Location: HomeView.php");
} else if (!empty($_POST) && !empty($_POST["Username"]) && !empty($_POST["Password"])) {
    $user = UserController::PopulateUser($_POST);
    if (!$auth->Login($user)) {
        echo "Wrong Login";
    } else {
        header("Location: HomeView.php");
    }
}
?>

<html>
<head>
    <link rel="stylesheet" href="../Context/bootstrap.css">
</head>
<body>
<div class="container">
    <h1>Login</h1>
    <form action="LoginView.php" method="post">
        <div class="form-group">
            <label for="Username">Username</label>
            <input class="form-control" id="Username" type="text" name="Username" required>
        </div>
        <div class="form-group">
            <label for="Password">Password</label>
            <input class="form-control" id="Password" type="password" name="Password" required>
        </div>
        <button type="submit" class="btn btn-primary">Login</button>
    </form>
    <a href="RegisterView.php">Register</a>
</div>
</body>
</html>
