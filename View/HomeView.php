<?php
include_once "../Controller/AuthenticationController.php";
include_once "../Controller/InseratController.php";
session_start();
$inseratController = new InseratController();

if (!AuthenticationController::CheckAuthentication()) {
    exit(header("Location: RegisterView.php"));
}
?>
<html>
<head>
    <link href="../Context/bootstrap.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h1>Home</h1>
    <nav class="navbar">
        <a href="UserProfileView.php">My Profile</a>
        <a href="MyInseratsView.php">My Inserats</a>
        <a href="Logout.php">Logout</a>
    </nav>
    <div class="list-group">
        <?php
        $inserate = $inseratController->SearchInserat();
        if (count($inserate) <= 0) {
            echo "<h2 class='list-group-item'>No Inserate</h2>";
        } else {
            for ($i = 0; $i < count($inserate); $i++) {
                echo "<a class='list-group-item' href='DetailView.php?id=" . $inserate[$i]->Id . "'>" . $inserate[$i]->Title . " | Price: " . $inserate[$i]->Price . " Dollars</a>";
            }
        }

        ?>
    </div>
</div>
</body>
</html>
