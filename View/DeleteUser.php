<?php
include_once "../Controller/AuthenticationController.php";
include_once "../Controller/UserController.php";
session_start();

$userControll = new UserController();
if (!AuthenticationController::CheckAuthentication()) {
    header("Location: HomeView.php");
} else if (AuthenticationController::CheckCSRF($_POST)) {
    $userControll->DeleteUser();
    header("Location: Logout.php");
} else {
    header("Location: Logout.php");
}
?>