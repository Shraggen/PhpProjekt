<?php
include_once "../Controller/AuthenticationController.php";
include_once "../Controller/UserController.php";
$auth = new AuthenticationController();

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (AuthenticationController::CheckAuthentication()) {
    header("Location: HomeView.php");
} else if (!empty($_POST)) {
    $user = UserController::PopulateUser($_POST);
    if (!$auth->Register($user)) {
        echo "Another User with the same Username already exists";
    } else {
        header("Location: LoginView.php");
    }
}
?>

<html>
<head>
    <link href="../Context/bootstrap.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h1>Register</h1>
    <form action="RegisterView.php" method="post">
        <div class="form-group">
            <label for="Surname">Surname</label>
            <input class="form-control" id="Surname" type="text" name="Surname" required>
        </div>
        <div class="form-group">
            <label for="Name">Name</label>
            <input class="form-control" id="Name" type="text" name="Name" required>
        </div>
        <div class="form-group">
            <label for="Email">Email</label>
            <input class="form-control" id="Email" type="email" name="Email" required>
        </div>
        <div class="form-group">
            <label for="Username">Username</label>
            <input class="form-control" id="Username" type="text" name="Username" required>
        </div>
        <div class="form-group">
            <label for="Password">Password</label>
            <input class="form-control" id="Password" type="password" name="Password" required>
        </div>
        <button class="btn btn-primary" type="submit">Register</button>
    </form>
    <a href="LoginView.php">Login</a>
</div>
</body>
</html>
