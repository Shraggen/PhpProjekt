<?php
include_once "../Controller/AuthenticationController.php";
include_once "../Controller/UserController.php";

session_start();

$userControl = new UserController();
$user;
if (!AuthenticationController::CheckAuthentication()) {
    header("Location: RegisterView.php");
} else if (!empty($_GET["Id"]) && $userControl->CheckIfUserIdBelongsToUser((int)$_GET["Id"])) {
    $user = $userControl->SearchUser((int)$_GET["Id"]);
    if (!empty($_POST) && AuthenticationController::CheckCSRF($_POST)) {
        $user = UserController::PopulateUser($_POST);
        $user->Id = (int)$_GET["Id"];
        $userControl->UpdateUser($user);
        exit(header("Location: HomeView.php"));
    }
} else {
    exit(header("Location: HomeView.php"));
}
?>
<html>
<head>
    <link rel="stylesheet" href="../Context/bootstrap.css">
</head>
<body>
<div class="container">
    <h1>Edit User <?php echo $user->Username ?></h1>
    <nav class="navbar">
        <a href="HomeView.php">Home</a>
        <a href="MyInseratsView.php">My Inserats</a>
        <a href="Logout.php">Logout</a>
    </nav>
    <form method="post">
        <input type="hidden" name="token" value="<?php echo $_SESSION["token"] ?>">
        <div class="form-group">
            <label for="Username">Username</label>
            <input class="form-control" id="Username" type="text" name="Username" value="<?php echo $user->Username ?>">
        </div>
        <div class="form-group">
            <label for="Surname">Surname</label>
            <input class="form-control" id="Surname" type="text" name="Surname" value="<?php echo $user->Surname ?>">
        </div>
        <div class="form-group">
            <label for="Name">Name</label>
            <input class="form-control" id="Name" type="text" name="Name" value="<?php echo $user->Name ?>">
        </div>
        <div class="form-group">
            <label for="Email">Email</label>
            <input class="form-control" id="Email" type="email" name="Email" value="<?php echo $user->Email ?>">
        </div>
        <div class="form-group">
            <label for="Password">Password</label>
            <input class="form-control" id="Password" type="password" name="Password">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
</html>
