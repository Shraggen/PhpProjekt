<?php
include_once "../Controller/AuthenticationController.php";
include_once "../Controller/InseratController.php";

session_start();

$inseratController = new InseratController();
$inserat;
if (!AuthenticationController::CheckAuthentication()) {
    exit(header("Location: RegisterView.php"));
} else if (!empty($_GET)) {
    $inserat = $inseratController->SearchInserat((int)$_GET["id"]);
    if (!empty($_POST) && !$inseratController->CheckIfInseratBelongsToUser((int)$inserat->Id) && !$inseratController->CheckIfInseratBought((int)$inserat->Id) && AuthenticationController::CheckCSRF($_POST)) {
        $inserat->BuyerId = $_SESSION["UserId"];
        $inseratController->UpdateInserat($inserat);
        exit(header("Location: MyInseratsView.php"));
    }
} else {
    exit(header("Location: HomeView.php"));
}
?>
<html>
<head>
    <link rel="stylesheet" href="../Context/bootstrap.css">
</head>
<body>
<div class="container">
    <h1>Detail View</h1>
    <nav class="navbar">
        <a href="HomeView.php">Home</a>
        <a href="UserProfileView.php">My Profile</a>
        <a href="MyInseratsView.php">My Inserats</a>
        <a href="Logout.php">Logout</a>
    </nav>
    <div class="jumbotron">
        <h1><?php echo $inserat->Title ?></h1>
        <p><?php echo $inserat->Description ?></p>
        <p>Price: <?php echo $inserat->Price ?></p>
    </div>
    <?php
    if ($inseratController->CheckIfInseratBelongsToUser($inserat->Id) == false && $inseratController->CheckIfInseratBought($inserat->Id) == false) {
        $token = $_SESSION["token"];
        echo "<form method='post'><input type='hidden' name='token' value='$token'><button class='btn btn-success' type='submit'>Buy Inserat</button></form>";
    }
    ?>
</div>
</body>
</html>
