<?php
include_once "../Controller/InseratController.php";
include_once "../Controller/AuthenticationController.php";

session_start();

$inseratController = new InseratController();
if (!AuthenticationController::CheckAuthentication()) {
    header("Location: RegisterView.php");
} else if (!empty($_GET) && $inseratController->CheckIfInseratBelongsToUser($_GET["Id"])) {
    $inseratController->DeleteInserat((int)$_GET["Id"]);
    header("Location: MyInseratsView.php");
}
?>