<?php
include_once "../Controller/AuthenticationController.php";
include_once "../Controller/InseratController.php";
session_start();
$inseratControl = new InseratController();
$inserat;
if (!AuthenticationController::CheckAuthentication()) {
    exit(header("RegisterView.php"));
}
if (!empty($_GET["Id"]) && $inseratControl->CheckIfInseratBelongsToUser((int)$_GET["Id"])) {
    $inserat = $inseratControl->SearchInserat((int)$_GET["Id"]);
    if (!empty($_POST) && AuthenticationController::CheckCSRF($_POST)) {
        $inserat = InseratController::PopulateInserat($_POST);
        $inserat->Id = (int)$_GET["Id"];
        $inseratControl->UpdateInserat($inserat);
        exit(header("Location: HomeView.php"));
    }
} else {
    exit(header("Location: HomeView.php"));
}
?>
<html>
<head>
    <link rel="stylesheet" href="../Context/bootstrap.css">
</head>
<body>
<div class="container">
    <h1>Edit Inserat <?php echo $inserat->Title ?></h1>
    <nav class="navbar">
        <a href="HomeView.php">Home</a>
        <a href="MyInseratsView.php">My Inserats</a>
        <a href="Logout.php">Logout</a>
    </nav>
    <form method="post">
        <input type="hidden" name="token" value="<?php echo $_SESSION["token"] ?>">
        <div class="form-group">
            <label for="Title">Title</label>
            <input class="form-control" id="Title" type="text" name="Title" value="<?php echo $inserat->Title ?>">
        </div>
        <div class="form-group">
            <label for="Price">Price</label>
            <input class="form-control" id="Price" type="number" name="Price" value="<?php echo $inserat->Price ?>">
        </div>
        <div class="form-group">
            <label for="Description">Description</label>
            <input class="form-control" id="Description" type="text" name="Description"
                   value="<?php echo $inserat->Description ?>">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
</body>
</html>
