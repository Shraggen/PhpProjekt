<?php
include_once "../Model/User.php";
include_once "../Model/Inserat.php";
class Database{
    public $db;
    //On Construction of an Object Creates a Database Connection on the Property  'db'
    public function __construct()
    {
        //Config
        $host = "localhost";
        $username = "root";
        $password = "";
        $dbName = "inser.at";
        $this->db = new mysqli($host, $username, $password, $dbName);
    }
    //Prepares a Statement which can include multiple parameters
    public function resultStatement($sql, $type, $params){
        $statement = $this->db->prepare($sql);
        if (!empty($params)){
            $statement->bind_param($type, ...$params);
        }
        $statement->execute();
        $result = $statement->get_result();
        //Returns the Statement with its result stored in it
        return $result;
    }
    public function ExtractData(mysqli_result $result, string $targetClass): array {
        $data = array();
        if ($result->num_rows > 0){
            if ($targetClass === "User"){
                while ($row = $result->fetch_assoc()){
                    array_push($data, User::FillUser($row));
                }
            } elseif ($targetClass === "Inserat"){
                while ($row = $result->fetch_assoc()){
                    array_push($data, Inserat::FillInserat($row));
                }
            }
        }
        return $data;
    }
}
?>