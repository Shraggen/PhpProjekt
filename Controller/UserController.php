<?php
include_once "../Database/Database.php";
include_once "../Model/User.php";
class UserController
{
    private $db;
    public function __construct()
    {
        $this->db = new Database();
    }

    //Public functions
    public function PostUser(User $postUser)
    {
        $sql = "INSERT INTO user (Username, Name, Surname, Email, Password) VALUES (?, ?, ?, ?, ?)";
        $this->db->resultStatement($sql, "sssss", array(
           $postUser->Username,
           $postUser->Name,
           $postUser->Surname,
           $postUser->Email,
           $postUser->Password
        ));
    }

    public function UpdateUser(User $updateUser)
    {
        $sql = "UPDATE user SET Username = ?, Surname = ?, Name = ?, Email = ?, Password = ? WHERE Id = ?";
        $this->db->resultStatement($sql, "sssssi", array(
           $updateUser->Username,
           $updateUser->Surname,
           $updateUser->Name,
           $updateUser->Email,
            hash("sha256", $updateUser->Password),
           $_SESSION["UserId"]
        ));
    }

    public function DeleteUser()
    {
        $sql = "DELETE FROM inserat WHERE OwnerId = ?";
        $this->db->resultStatement($sql, "i", array($_SESSION["UserId"]));
        $sql = "DELETE FROM user WHERE Id = ?";
        $this->db->resultStatement($sql, "i", array($_SESSION["UserId"]));
    }

    public function CheckIfUserIdBelongsToUser(int $userId): bool
    {
        $sql = "SELECT * FROM user WHERE Id = ?";
        $result = $this->db->resultStatement($sql, "i", array($userId));
        $data = $this->db->ExtractData($result, "User");
        if ($data[0]->Id == $_SESSION["UserId"])
        {
            return true;
        }
        return false;
    }
    public function SearchUser(int $searchId = 0)
    {
        //Todo
        if ($searchId == 0){
            $sql = "SELECT * FROM user";
            $result = $this->db->resultStatement($sql, null, null);
            return $this->db->ExtractData($result, "User");
        } else{
            $sql = "SELECT * FROM user WHERE Id = ?";
            $result = $this->db->resultStatement($sql, "i", array($searchId));
            return $this->db->ExtractData($result, "User")[0];
        }
    }

    //Static functions
    public static function PopulateUser($post): User
    {
        $user = new User();
        $user->Name = $post["Name"];
        $user->Username = $post["Username"];
        $user->Password = $post["Password"];
        $user->Email = $post["Email"];
        $user->Surname = $post["Surname"];
        return $user;
    }
}
?>