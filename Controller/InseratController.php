<?php
include_once "../Database/Database.php";
include_once "../Model/Inserat.php";

class InseratController
{
    private $db;

    //Constructor
    public function __construct()
    {
        $this->db = new Database();
    }

    //Public functions
    public function PostInserat(Inserat $postInserat)
    {
        $sql = "INSERT INTO inserat (Title, Description, Price, OwnerId) VALUES (?, ?, ?, ?)";
        $this->db->resultStatement($sql, "sssi", array(
            $postInserat->Title,
            $postInserat->Description,
            $postInserat->Price,
            $_SESSION["UserId"]
        ));
    }

    public function UpdateInserat(Inserat $updateInserat)
    {
        $sql = "UPDATE inserat SET Description = ?, Price = ?, Title = ?, BuyerId = ? WHERE Id = ?";
        $this->db->resultStatement($sql, "sdsii", array(
            $updateInserat->Description,
            (double)$updateInserat->Price,
            $updateInserat->Title,
            (int)$updateInserat->BuyerId,
            (int)$updateInserat->Id
        ));
    }

    public function DeleteInserat($inseratId)
    {
        $sql = "DELETE FROM inserat WHERE Id = ?";
        $this->db->resultStatement($sql, "i", array($inseratId));
    }

    public function SearchInserat($searchId = 0)
    {
        if ($searchId === 0) {
            $sql = "SELECT * FROM inserat";
            $result = $this->db->resultStatement($sql, null, null);
            $data = $this->db->ExtractData($result, "Inserat");
            return $data;
        } else {
            $sql = "SELECT * FROM inserat WHERE Id = ?";
            $result = $this->db->resultStatement($sql, "i", array($searchId));
            $data = $this->db->ExtractData($result, "Inserat");
            return $data[0];
        }
    }

    public function SearchMyInserate()
    {
        $sql = "SELECT * FROM inserat WHERE OwnerId = ?";
        $result = $this->db->resultStatement($sql, "i", array((int)$_SESSION["UserId"]));
        $data = $this->db->ExtractData($result, "Inserat");
        return $data;
    }

    public function SearchBoughtInserate()
    {
        $sql = "SELECT * FROM inserat WHERE BuyerId = ?";
        $result = $this->db->resultStatement($sql, "i", array((int)$_SESSION["UserId"]));
        $data = $this->db->ExtractData($result, "Inserat");
        return $data;
    }

    public function CheckIfInseratBelongsToUser(int $id): bool
    {
        $sql = "SELECT * FROM inserat WHERE Id = ?";
        $result = $this->db->resultStatement($sql, "i", array($id));
        $data = $this->db->ExtractData($result, "Inserat");
        if ($data[0]->OwnerId === (int)$_SESSION["UserId"]) {
            return true;
        }
        return false;
    }

    public function CheckIfInseratBought(int $id): bool
    {
        $sql = "SELECT * FROM inserat WHERE Id = ?";
        $result = $this->db->resultStatement($sql, "i", array($id));
        $data = $this->db->ExtractData($result, "Inserat");
        if ($data[0]->BuyerId === (int)$_SESSION["UserId"])
        {
            return true;
        }
        return false;
    }
    //Static functions
    public static function PopulateInserat($post): Inserat
    {
        $inserat = new Inserat();
        $inserat->Title = $post["Title"];
        $inserat->Price = (double)$post["Price"];
        $inserat->Description = $post["Description"];
        $inserat->OwnerId = (int)$_SESSION["UserId"];
        return $inserat;
    }
}