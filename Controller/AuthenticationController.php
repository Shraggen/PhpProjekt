<?php
include_once "../Database/Database.php";
class AuthenticationController
{
    private $db;

    //Constructor
    public function __construct()
    {
        $this->db = new Database();
    }

    //Public functions
    public function Login(User $user): bool
    {
        if (isset($user->Username, $user->Password) && !$this->CheckAuthentication()) {
            if (!isset($_SESSION["token"])) {
                $_SESSION["token"] = bin2hex(random_bytes(32));
            }
            $id = $this->UserAndPasswordMatch($user->Username, $user->Password);
            if ($id === 0) {
                return false;
            }
            $_SESSION["UserId"] = $id;
            return true;
        } else {
            return false;
        }
    }

    public function Register(User $user): bool
    {
        if ($this->ValidateUser($user) && !$this->CheckIfUserExists($user->Username)) {
            $sql = "INSERT INTO user (Username, Surname, Name, Email, Password) VALUES (?, ?, ?, ?, ?)";
            $userArray = array(
                $user->Username,
                $user->Surname,
                $user->Name,
                $user->Email,
                hash("sha256", $user->Password)
            );
            $this->db->resultStatement($sql, "sssss", $userArray);
            $id = $this->db->db->query("SELECT LAST_INSERT_ID() as Id")->fetch_assoc()["Id"];
            session_start();
            $_SESSION["token"] = bin2hex(random_bytes(32));
            $_SESSION["UserId"] = $id;
            return true;
        } else {
            return false;
        }
    }

    public function Logout()
    {
        session_destroy();
    }

    //Private functions
    private function UserAndPasswordMatch($username, $password): int
    {
        $sql = "SELECT Id, Username, Password FROM user WHERE Username = ? AND Password = ?";
        $result = $this->db->resultStatement($sql, "ss", array($username, hash("sha256", $password)));
        $data = $this->db->ExtractData($result, "User");
        if ($data[0]->Id) {
            return $data[0]->Id;
        }
        return 0;
    }

    private function CheckIfUserExists($username): bool
    {
        $sql = "SELECT Username FROM user WHERE Username = ?";
        $result = $this->db->resultStatement($sql, "s", array($username));
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function ValidateUser(User $user): bool
    {
        if (isset($user->Name, $user->Surname, $user->Email, $user->Password, $user->Username)) {
            return true;
        }
        return false;
    }

    //Static functions
    public static function CheckCSRF($post): bool
    {
        if (isset($post["token"]) && $post["token"] === $_SESSION["token"]){
            return true;
        }
        session_destroy();
        return false;
    }

    public static function CheckAuthentication(): bool
    {
        if (isset($_SESSION["UserId"]) && $_SESSION["UserId"] == true) {
            return true;
        }
        return false;
    }
}

?>